# Sendinblue subscription manager

A tool to manage email subscription to different lists in a Sendinblue folder.

## Installation

Clone the repository in read only

```bash
git clone https://framagit.org/colibris/sendinblue-subscription-manager.git
```

Get the dependencies using [composer](https://getcomposer.org).

```bash
cd sendinblue-subscription-manager
composer install -o
```

## Configuration

Copy the .env.example file into .env

```bash
cp .env.example .env
```

Edit the .env configuration file and set at least :

- `SENDINBLUE_API_KEY` : the api key generate in your sendinblue control panel
- `SENDINBLUE_FOLDER_ID` : the id of the folder containing your lists (1 by default)
