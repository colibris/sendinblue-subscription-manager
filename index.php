<?php
error_reporting(E_ALL);
require_once(__DIR__ . '/vendor/autoload.php');

if (file_exists('.env')) {
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
    $dotenv->load();
} else {
    exit('ERROR : no .env file found, please copy .env.example to .env and change the values.');
}
if (!empty($_ENV['SENDINBLUE_API_KEY']) && !empty($_ENV['SENDINBLUE_FOLDER_ID'])) {
    define('SENDINBLUE_API_KEY', $_ENV['SENDINBLUE_API_KEY']);
    define('SENDINBLUE_FOLDER_ID', $_ENV['SENDINBLUE_FOLDER_ID']);
} else {
    exit('ERROR : no value for SENDINBLUE_API_KEY and/or SENDINBLUE_FOLDER_ID, edit the .env file and add the values.');
}

// Configure API key authorization: api-key
$config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', SENDINBLUE_API_KEY);

$contactsApi = new SendinBlue\Client\Api\ContactsApi(
    new GuzzleHttp\Client(),
    $config
);

$listsApi = new SendinBlue\Client\Api\ListsApi(
    new GuzzleHttp\Client(),
    $config
);

// Enable debugging
phpCAS::setLogger();
// Enable verbose error messages. Disable in production!
phpCAS::setVerbose($_ENV['CAS_VERBOSE_MESSAGES']);

// Initialize phpCAS
phpCAS::client(
    CAS_VERSION_2_0,
    $_ENV['CAS_HOST'],
    (int) $_ENV['CAS_PORT'],
    $_ENV['CAS_CONTEXT']
);

// logout if desired
if (isset($_REQUEST['logout'])) {
    phpCAS::logout();
}

// For production use set the CA certificate that is the issuer of the cert
// on the CAS server and uncomment the line below
// phpCAS::setCasServerCACert($cas_server_ca_cert_path);
// For quick testing you can disable SSL validation of the CAS server.
// THIS SETTING IS NOT RECOMMENDED FOR PRODUCTION.
// VALIDATING THE CAS SERVER IS CRUCIAL TO THE SECURITY OF THE CAS PROTOCOL!
phpCAS::setNoCasServerValidation();
// force CAS authentication
phpCAS::forceAuthentication();

$adminUsers = array_map('trim', explode(',', $_ENV['ADMIN_GROUP']));
$userIsAdmin = in_array(phpCAS::getUser(), $adminUsers);

$output = '';

if ($userIsAdmin) {
    $mail = (!empty($_GET['mail']) ? $_GET['mail'] : phpCAS::getAttribute('mail'));
    $output .= '<form method="GET">
            <div class="row">
                <div class="col-xs-8 col-sm-9 col-md-9 col-lg-10">
                    <input type="email" name="mail" placeholder="Email" value="'.$mail.'" />
                </div>
                <div class="col-xs-4 col-sm-3 col-md-3 col-lg-2">
                    <input type="submit" class="secondary" value="Rechercher" />
                </div>
            </div>
        </form>';
} else {
    $mail = phpCAS::getAttribute('mail');
}
// $output .= '<p>Connecté·e en tant que '.phpCAS::getUser().' - mail '.phpCAS::getAttribute('mail').'</b>.</p>';
// $output .= '<p><a href="?logout=" class="secondary" role="button">Se déconnecter</a></p>';

$usersubscribedLists = [];
if (!empty($mail)) {
    $limit = 20; // int | Number of documents per page
    $offset = 0; // int | Index of the first document of the page
    $sort = "desc"; // string | Sort the results in the ascending/descending order of record creation. Default order is **descending** if `sort` is not passed

    $usersubscribedLists = $contactsApi->getContactInfo($mail)->getlistIds();

    //si ya des opération d'inscription
    if (isset($_GET['action']) && $_GET['action']=='subscribe' && !empty($_GET['id'])) {
        try {
            $contactEmails = new \SendinBlue\Client\Model\AddContactToList(
                [
                    'emails' => [$mail]
                ]
            );
            $listsApi->addContactToList($_GET['id'], $contactEmails);
            header('Location: ?mail='.$mail);
        } catch (Exception $e) {
            exit('Exception when calling ListsApi->addContactToList: '. $e->getMessage(). PHP_EOL);
        }
    }
    //si ya des opération de désinscription
    if (isset($_GET['action']) && $_GET['action']=='unsubscribe') {
        try {
            $contactEmails = new \SendinBlue\Client\Model\RemoveContactFromList(
                [
                    'emails' => [$mail]
                ]
            );
            $listsApi->removeContactFromList($_GET['id'], $contactEmails);
            header('Location: ?mail='.$mail);
        } catch (Exception $e) {
            exit('Exception when calling ListsApi->removeContactFromList: '. $e->getMessage(). PHP_EOL);
        }
    }

    try {
        $result = $contactsApi->getFolderLists(SENDINBLUE_FOLDER_ID, $limit, $offset, $sort);
    } catch (Exception $e) {
        exit('Exception when calling ContactsApi->getFolderLists: '. $e->getMessage(). PHP_EOL);
    }

    foreach ($result->getLists() as $list) {
        $buttons = '';
        if (!empty($mail) && in_array($list['id'], $usersubscribedLists)) {
            if ($mail == phpCAS::getAttribute('mail')) {
                $label = 'Se désabonner';
            } else {
                $label = 'Désabonner<br /><small>'.$mail.'</small>';
            }
            $buttons = '<form class="form-sub-button" method="POST" action="?mail='.urlencode($mail).'&id='.$list['id'].'&action=unsubscribe">
            <button type="submit" class="secondary">'.$label.'</button>
        </form>';
        } elseif (!empty($mail)) {
            if ($mail == phpCAS::getAttribute('mail')) {
                $label = 'S\'abonner';
            } else {
                $label = 'Abonner<br /><small>'.$mail.'</small>';
            }
            $buttons = '<form class="form-sub-button" method="POST" action="?mail='.urlencode($mail).'&id='.$list['id'].'&action=subscribe">
            <button type="submit">'.$label.'</button>
        </form>';
        }
        $output .=  '<article><div class="row middle-xs">
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
            <h5>'.$list['name'].'</h5>
            <small>'.$list['uniqueSubscribers'].' inscrit·es</small>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            '.$buttons.'
        </div>
    </div></article>';
    }
}

?>
<!doctype html>
<html lang="fr" data-theme="light">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/pico.min.css">
    <link rel="stylesheet" href="css/flexboxgrid.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title>Gestion des abonnements aux lettres Colibris</title>
  </head>
  <body>
      <main class="container">
    <nav style="margin-top: -4em;margin-bottom: 2em;">
        <ul>
            <li><strong><?php echo ($userIsAdmin) ? 'Gestion des abonnements' : 'Mes abonnements'; ?></strong></li>
        </ul>
        <ul>
            <li><?php echo phpCAS::getUser(); ?></li>
            <li><a href="?logout">Se déconnecter</a></li>
        </ul>
    </nav>
        <!-- <hgroup>
            <h2>Mes abonnements</h2>
            <h3>Choisir vos envois de lettres Colibris</h3>
        </hgroup> -->
        <?php echo $output; ?>
    </main>
  </body>
</html>